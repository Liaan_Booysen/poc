namespace Data.Infrastructure
{
    public interface IDatabaseFactory
    {
        DataContext Get();
    }
}