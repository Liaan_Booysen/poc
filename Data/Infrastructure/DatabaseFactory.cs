﻿namespace Data.Infrastructure
{
    public class DatabaseFactory : Disposable, IDatabaseFactory
    {
        private readonly string _nameOrConnectionString;
        private DataContext _dataContext;

        public DatabaseFactory(string nameOrConnectionString)
        {
            _nameOrConnectionString = nameOrConnectionString;
        }

        public DataContext Get()
        {
            return _dataContext ?? (_dataContext = new DataContext(_nameOrConnectionString));
        }

        protected override void DisposeCore()
        {
            if (_dataContext != null)
                _dataContext.Dispose();
        }
    }
}