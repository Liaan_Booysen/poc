﻿using System.Data.Entity;

namespace Data.Infrastructure
{
    public class UnitOfWork : IUnitOfWork
    {
        private DbContext _dataContext;
        private readonly IDatabaseFactory _databaseFactory;

        public UnitOfWork(IDatabaseFactory databaseFactory)
        {
            _databaseFactory = databaseFactory;
        }

        protected DbContext DataContext
        {
            get { return _dataContext?? (_dataContext = _databaseFactory.Get()); }
        }

        public void Commit()
        {
            DataContext.SaveChanges();
        }
    }
}