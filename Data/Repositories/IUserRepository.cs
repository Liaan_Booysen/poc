﻿using Data.Infrastructure;
using Model.Entity;

namespace Data.Repositories
{
    public interface IUserRepository : IRepository<User>
    {
    }
}