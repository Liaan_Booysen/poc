﻿using System.Data.Entity;
using Model.Entity;

namespace Data
{
    public class DataContext : DbContext
    {
        //DBSet
        public DbSet<User> Categories { get; set; }

        public DataContext(string nameOrConnectionString)
            :base(nameOrConnectionString)
    {
    }

        public virtual void Commit()
        {
            SaveChanges();
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            //modelBuilder
        }
    }
}