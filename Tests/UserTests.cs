﻿using System.Net;
using Api.Controllers;
using Autofac;
using Model.Entity;
using NUnit.Framework;
using Tests.Helper;

namespace Tests
{
    //As a system administrator
    //I want to be able to manage users
    //So that the user database is well maintaned
    [TestFixture]
    public class UserTests
    {
        private IContainer _container;

        const string FirstName = "Liaan";
        const string Gender = "Male";
        const string Surname = "Booysen";
        const string Email = "liaanbooysen@gmail.com";

        [SetUp]
        public void Setup()
        {
            _container = AutoFacInitialiser.AutoFacSetup();
        }

        //Given:  I have a user email, name, surname and gender
        //When:   I create the user
        //Then:   I should be able to retrieve it
        [Test]
        public async void AddAnewUserTest()
        {
            using (var lifetime = _container.BeginLifetimeScope())
            {
                //given
                var user = GetTestingUser();

                //when
                var controller = APIControllerTestHelper.ResolveController<UserController>(lifetime);
                controller.SaveUser(user);

                //then
                var retrievedUser =
                    await APIControllerTestHelper.GetContent<User>(controller.GetUserByEmailAddress(Email));
                Assert.AreEqual(user.Email, retrievedUser.Email, "The email addresses are not equal");

                //Cleanup
                controller.DeleteUser(user);
            }
        }

        //Given:  I have a user
        //When:   I delete the user
        //Then:   I should not able to retrieve it
        [Test]
        public void DeleteAUserTest()
        {
            using (var lifetime = _container.BeginLifetimeScope())
            {
                //given
                var user = GetTestingUser();
                var controller = APIControllerTestHelper.ResolveController<UserController>(lifetime);
                controller.SaveUser(user);

                //when
                controller.DeleteUser(user);

                //then
                var retrievedUser = controller.GetUserByEmailAddress(Email);
                Assert.AreEqual(HttpStatusCode.NoContent, retrievedUser.StatusCode, "The user was found.");
            }
        }

        //Given I have few users
        //When  I retrieve them all
        //Then  The count should match with the expected values
        [Test]
        public async void GetAllUsers()
        {
            using (var lifetime = _container.BeginLifetimeScope())
            {
                //given
                const int numberOfUsers = 3;
                var user = GetTestingUser();
                var controller = APIControllerTestHelper.ResolveController<UserController>(lifetime);
                for (var i = 0; i < numberOfUsers; i++)
                    controller.SaveUser(user);

                //when
                var users = await APIControllerTestHelper.GetContentList<User>(controller.GetAll());

                //then
                Assert.AreEqual(users.Count, numberOfUsers, "The incorrect number of users were found.");

                //Cleanup
                foreach (var userToBeDeleted in users)
                {
                    controller.DeleteUser(userToBeDeleted);
                }
            }
        }

        //Given I have no users in the database
        //When I try to retrieve the users
        //Then I should receive a message with no content
        [Test]
        public async void GetAllUsersButNoneExist()
        {
            using (var lifetime = _container.BeginLifetimeScope())
            {
                //given
                var controller = APIControllerTestHelper.ResolveController<UserController>(lifetime);
                var precheckUsers = controller.GetAll();
                if (precheckUsers.StatusCode != HttpStatusCode.Gone)
                {
                    var usersToBeDeleted = await APIControllerTestHelper.GetContentList<User>(precheckUsers);
                    foreach (var userToBeDeleted in usersToBeDeleted)
                    {
                        controller.DeleteUser(userToBeDeleted);
                    }
                }

                //when
                var response = controller.GetAll();

                //then
                Assert.AreEqual(HttpStatusCode.Gone, response.StatusCode, "The incorrect status code was returned.");
            }
        }

        private User GetTestingUser()
        {
            return new User
                   {
                       FirstName = FirstName,
                       Gender = Gender,
                       Surname = Surname,
                       Email = Email
                   };
        }
    }
}
