﻿using System.Data.Entity;
using Data.Infrastructure;

namespace Tests.Helper
{
    public class TestUnitOfWork : IUnitOfWork
    {
        private DbContext _dataContext;
        private readonly IDatabaseFactory _databaseFactory;

        public TestUnitOfWork(IDatabaseFactory databaseFactory)
        {
            _databaseFactory = databaseFactory;
        }

        protected DbContext DataContext
        {
            get { return _dataContext ?? (_dataContext = _databaseFactory.Get()); }
        }

        public void Commit()
        {
            //Do nothing
        }
    }
}