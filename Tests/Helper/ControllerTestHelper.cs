﻿using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Hosting;
using Autofac;

namespace Tests.Helper
{
    public static class APIControllerTestHelper
    {
        public static T ResolveController<T>(IComponentContext lifetime) where T : ApiController
        {
            var controller = lifetime.Resolve<T>();
            controller.Request = new HttpRequestMessage();
            controller.Request.Properties.Add(HttpPropertyKeys.HttpConfigurationKey, new HttpConfiguration());
            return controller;
        }

        public static async Task<T> GetContent<T>(HttpResponseMessage message)
        {
            return await message.Content.ReadAsAsync<T>();
        }

        public static async Task<List<T>> GetContentList<T>(HttpResponseMessage message)
        {
            return await message.Content.ReadAsAsync<List<T>>();
        }
    }
}