﻿using System.Reflection;
using System.Web.Http;
using Api.Controllers;
using Autofac;
using Autofac.Integration.WebApi;
using Data.Infrastructure;
using Data.Repositories;
using Service;
  
namespace Tests.Helper
{
    public static class AutoFacInitialiser
    {
        public static IContainer AutoFacSetup()
        {
            var builder = new ContainerBuilder();
            builder.RegisterType<DatabaseFactory>().As<IDatabaseFactory>().WithParameter("nameOrConnectionString", "UserDB").SingleInstance();
            builder.RegisterType<UnitOfWork>().As<IUnitOfWork>().SingleInstance();
            builder.RegisterAssemblyTypes(typeof(UserRepository)
                .Assembly).Where(t => t.Name.EndsWith("Repository"))
                .AsImplementedInterfaces().SingleInstance();
            builder.RegisterType<AmIHereController>().As(typeof(AmIHereController)).SingleInstance();
            builder.RegisterType<UserController>().As(typeof(UserController)).SingleInstance();
            builder.RegisterType<UserService>().As<IUserService>().SingleInstance();
            return builder.Build();
        }
    }
}