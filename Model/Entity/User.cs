﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Model.Entity
{
    public class User
    {
        public User()
        {
            DateCreated = DateTime.Now;
        }

        [Key]
        public int UserId { get; set; }

        [Required]
        [EmailAddress]
        public string Email { get; set; }

        [Required]
        public string FirstName { get; set; }

        [Required]
        public string Surname { get; set; }

        [Required]
        public string Gender { get; set; }

        public DateTime DateCreated { get; set; }
    }
}