﻿using System.Collections.Generic;
using System.Linq;
using Data.Infrastructure;
using Data.Repositories;
using Model.Entity;

namespace Service
{
    public class UserService : IUserService
    {
        private readonly IUserRepository _repository;
        private readonly IUnitOfWork _unitOfWork;

        public UserService(IUserRepository repository, IUnitOfWork unitOfWork)
        {
            _repository = repository;
            _unitOfWork = unitOfWork;
        }

        public void SaveUser(User user)
        {
            _repository.Add(user);
            _unitOfWork.Commit();
        }

        public User GetUserByEmail(string emailAddress)
        {
            return _repository.Get(x => x.Email == emailAddress);
        }

        public void DeleteUser(int userId)
        {
            _repository.Delete(x => x.UserId == userId);
            _unitOfWork.Commit();
        }

        public List<User> GetAll()
        {
            return _repository.GetAll().ToList();
        }
    }
}
