﻿using System.Collections.Generic;
using Model.Entity;

namespace Service
{
    public interface IUserService
    {
        void SaveUser(User user);
        User GetUserByEmail(string emailAddress);
        void DeleteUser(int userId);
        List<User> GetAll();
    }
}