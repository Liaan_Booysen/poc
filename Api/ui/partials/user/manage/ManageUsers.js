var manageUserController = myApp.controller('ManageUserController', ['$scope', '$rootScope', function ($scope, $rootScope) {
    $scope.hasError = false;
    $scope.showNotification = false;
    $scope.errorMessage = "";
    $scope.loading = false;

    $scope.users = [];
    $scope.gridOptions = { data: "users" };
    $scope.filterOptions = {
        filterText: ""
    };
    var itemBeingDeleted;

    //Plugins
    //http://plnkr.co/edit/LRdkiRzFjoe8LFK7Sxt7?p=preview
    var filterBarPlugin = {
        init: function (scope, grid) {
            filterBarPlugin.scope = scope;
            filterBarPlugin.grid = grid;
            $scope.$watch(function () {
                var searchQuery = "";
                angular.forEach(filterBarPlugin.scope.columns, function (col) {
                    if (col.visible && col.filterText) {
                        var filterText = col.filterText + "; ";
                        searchQuery += col.displayName + ": " + filterText;
                    }
                });
                return searchQuery;
            }, function (searchQuery) {
                filterBarPlugin.scope.$parent.filterText = searchQuery;
                filterBarPlugin.grid.searchProvider.evalFilter();
            });
        },
        scope: undefined,
        grid: undefined
    };

    $scope.myHeaderCellTemplate = '<div class="ngHeaderSortColumn {{col.headerClass}}" ng-style="{cursor: col.cursor}" ng-class="{ ngSorted: !noSortVisible }">' +
                           '<div ng-click="col.sort($event)" ng-class="\'colt\' + col.index" class="ngHeaderText">{{col.displayName}}</div>' +
                           '<div class="ngSortButtonDown" ng-show="col.showSortButtonDown()"></div>' +
                           '<div class="ngSortButtonUp" ng-show="col.showSortButtonUp()"></div>' +
                           '<div class="ngSortPriority">{{col.sortPriority}}</div>' +
                         '</div>' +
                         '<input type="text" ng-click="stopClickProp($event)" placeholder="Filter..." ng-model="col.filterText" ng-style="{ \'width\' : col.width - 14 + \'px\' }" style="position: absolute; top: 30px; bottom: 30px; left: 0; bottom:0;"/>' +
                         '<div ng-show="col.resizable" class="ngHeaderGrip" ng-click="col.gripClick($event)" ng-mousedown="col.gripOnMouseDown($event)"></div>';


    $scope.editableInPopup = '<a ng-click="delete(row)" >Delete</a> ';


    var colDefs = [{ field: 'UserId', displayName: 'ID', headerCellTemplate: $scope.myHeaderCellTemplate },
        { field: 'FirstName', displayName: 'Name', headerCellTemplate: $scope.myHeaderCellTemplate },
        { field: 'Surname', displayName: 'Surname', headerCellTemplate: $scope.myHeaderCellTemplate },
        { field: 'Email', displayName: 'EmailAddress', headerCellTemplate: $scope.myHeaderCellTemplate },
        { field: 'DateCreated', displayName: 'Created', type: 'date' },
        { displayName: 'Delete', cellTemplate: $scope.editableInPopup }];

    $scope.gridOptions = {
        data: 'users',
        columnDefs: colDefs,
        showGroupPanel: true,
        jqueryUIDraggable: true,
        plugins: [filterBarPlugin],
        filterOptions: $scope.filterOptions,
        headerRowHeight: 60 // give room for filter bar,
    };

    //Helper
    function toggleLoading() {
        $scope.loading = !$scope.loading;
    }

    function toggleNotification() {
        $scope.showNotification = !$scope.showNotification;
    }

    function showNotificationTemporarily() {
        toggleNotification();
        setTimeout(this.toggleNotification, 3000);
    }

    //Callbacks
    function successfullyRetrievedAllUsers(data) {
        $scope.users = data;
        toggleLoading();
    };

    function showError(message, data) {
        $scope.errorMessage = message;
        console.log(data);
        $scope.hasError = true;
        toggleLoading();
    }

    function errorCallback(data) {
        showError( "An Error has occured loading the data.  If this persists please contact the system administrator.", data);
    };

    function errorNoDataFoundCallback(data) {
        showError("No data has been found, please add some.", data);
    };

    function successfullyDeletedItem() {
        $scope.users.splice(itemBeingDeleted, 1);
        if (!$scope.users.count < 1) {
            errorNoDataFoundCallback();
        }
        showNotificationTemporarily();
        toggleLoading();
    };

    //Functions
    $scope.init = function() {
        toggleLoading();
        $rootScope.restService.call("User/GetAll", "GET", null, successfullyRetrievedAllUsers, errorNoDataFoundCallback);
    };

    $scope.isLoading = function () {
        return $scope.loading;
    };

    $scope.isNotLoading = function () {
        return !$scope.loading;
    };

    $scope.delete = function(row) {
        toggleLoading();
        itemBeingDeleted = $scope.users.indexOf(row.entity);
        $rootScope.restService.call("User/DeleteUser", "POST", row.entity, successfullyDeletedItem, errorCallback);
    }

    $scope.hasData = function () {
        return $scope.users.length > 0;
    };

    $scope.hasNoData = function () {
        return $scope.users.length < 1;
    };

    $scope.init();

}]);

