var addNewUserController = myApp.controller('NewUserController', ['$scope', '$rootScope','$route', function ($scope, $rootScope,$route) {
    $scope.hasError = false;
    $scope.errorMessage = "";
    $scope.loading = false;
    $scope.showMessage = false;

    //helper functions
    function setError(message) {
        $scope.errorMessage = message;
        $scope.hasError = true;
    }

    function toggleLoading() {
        $scope.loading = !$scope.loading;
    }

    function toggleShowMessage() {
        $scope.showMessage = !$scope.showMessage;
    }

    function hideErrorMessage() {
        $scope.errorMessage = "";
        $scope.hasError = false;
    }

    //callbacks
    function errorCallback(data) {
        console.log(data);
        setError("An error has occured while communicating with the Server.  If this persists please contact the system administrators.");
        toggleLoading();
    }

    function successfullySavedUser() {
        toggleShowMessage();
        toggleLoading();
    }

    //Fuctions
    $scope.save = function (user) {
        if ($scope.userForm.$valid) {
            hideErrorMessage();
            toggleLoading();
            $rootScope.restService.call("User/SaveUser", "POST", user, successfullySavedUser, errorCallback);
        } else {
            setError("There are invalid fields, please enter all the fields that have a red outline.");
        }
    };

    $scope.isLoading = function() {
        return $scope.loading;
    };

    $scope.isNotLoading = function () {
        return !$scope.loading;
    };

    $scope.shouldShowMessage = function () {
        return $scope.showMessage;
    };

    $scope.shouldShowAddDialog = function () {
        return !$scope.showMessage;
    };

    $scope.reset = function() {
        $route.reload();
    }
}]);