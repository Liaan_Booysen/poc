'use strict';

// declare top-level module which depends on filters,and services
var myApp = angular.module('myApp',
    [   'myApp.filters',
        'myApp.directives', // custom directives
        'ngGrid', // angular grid
        'ui', // angular ui
        'ngSanitize', // for html-bind in ckeditor
        'ui.ace', // ace code editor
        'ui.bootstrap', // jquery ui bootstrap
        '$strap.directives' // angular strap
    ]);


var filters = angular.module('myApp.filters', []);
var directives = angular.module('myApp.directives', []);

// bootstrap angular
myApp.config(['$routeProvider', '$locationProvider', '$httpProvider', function ($routeProvider, $locationProvider, $httpProvider) {

    // TODO use html5 *no hash) where possible
    // $locationProvider.html5Mode(true);

    $routeProvider.when('/', {
        templateUrl:'partials/home.html'
    });

    $routeProvider.when('/manageUsers', {
        templateUrl: 'partials/user/manage/manageUsers.html'
        , controller: 'ManageUserController'
    });

    $routeProvider.when('/newUser', {
        templateUrl: 'partials/user/add/newUser.html'
        ,controller: 'NewUserController'
    });

    // by default, redirect to site root
    $routeProvider.otherwise({
        redirectTo:'/'
    });

}]);

// this is run after angular is instantiated and bootstrapped
myApp.run(function ($rootScope, $location, $http, $timeout, RESTService) {
    $rootScope.restService = RESTService;
    $rootScope.apiURL = 'http://localhost:43074/api/';

    // instantiate and initialize an auth notification manager
    $rootScope.authNotifier = new NotificationManager($rootScope);

    // when user logs in, redirect to home
    $rootScope.authNotifier.notify("information", "Welcome to the practical question!");
});