'use strict';

// simple stub that could use a lot of work...
myApp.factory('RESTService',
    function ($rootScope,$http) {
        return {
            call: function (url, verb, parameters, callback, errorCallback) {
                return $http({ method: verb, url: $rootScope.apiURL + url, data: parameters }).
                    success(function (data, status, headers, config) {
                        callback(data);
                    }).
                    error(function (data, status, headers, config) {
                        errorCallback(data);
                });
            }
        };
    }
);