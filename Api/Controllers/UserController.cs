﻿using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Model.Entity;
using Service;

namespace Api.Controllers
{
    public class UserController : ApiController
    {
        private readonly IUserService _service;

        public UserController(IUserService service)
        {
            _service = service;
        }

        //Contract:
        //Save success 200
        //Error 500
        //TODO:  Out of scope for the task, check if the user has a unique email address (sorry not enough time)
        public HttpResponseMessage SaveUser(User user)
        {
            if(!ModelState.IsValid)
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            
            //save
            _service.SaveUser(user);

            return Request.CreateResponse(HttpStatusCode.OK, "Successfully saved the user");
        }

        //Contract:
        //Success 200 + User
        //Error 500
        public HttpResponseMessage GetUserByEmailAddress(string emailAddress)
        {
            var user = _service.GetUserByEmail(emailAddress);

            //if no user
            if (user == null)
                return Request.CreateErrorResponse(HttpStatusCode.NoContent,
                    "No user could be found with that email address");
            
            return Request.CreateResponse(HttpStatusCode.OK, user);
        }

        //Contract:
        //Success 200
        //Error 500
        [HttpPost]
        public HttpResponseMessage DeleteUser(User user)
        {
            _service.DeleteUser(user.UserId);
            return Request.CreateResponse(HttpStatusCode.OK, "User successfully deleted.");
        }

        //Contract:
        //Success 200
        //No users 410
        //Error 500
        public HttpResponseMessage GetAll()
        {
            var users = _service.GetAll();

            //if any
            if (users.Any())
                return Request.CreateResponse(HttpStatusCode.OK, users);
            
            //none
            return Request.CreateErrorResponse(HttpStatusCode.Gone, "No users could be found.");
        }
    }
}
