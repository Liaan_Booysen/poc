﻿using System.Web.Http;

namespace Api.Controllers
{
    public class AmIHereController : ApiController
    {
        public bool GetIAmHere()
        {
            return true;
        }
    }
}