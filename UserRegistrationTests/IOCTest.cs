﻿using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Hosting;
using Autofac;
using FeelMe.Helper;
using FeelMeApi.Controllers;
using NUnit.Framework;

namespace UserRegistrationTests
{
    //As a software developer
    //I want a simple test that will only do one thing
    //So that when I test the application and almost all the tests fail, I can use this test as a first port of call
    [TestFixture]
    public class IOCTest
    {
        private IContainer _container;

        [SetUp]
        public void Setup()
        {
            _container = AutoFacInitialiser.AutoFacSetup();
        }

        private static AmIHereController GetAmIHereController(IComponentContext lifetime)
        {
            var controller = lifetime.Resolve<AmIHereController>();
            controller.Request = new HttpRequestMessage();
            controller.Request.Properties.Add(HttpPropertyKeys.HttpConfigurationKey, new HttpConfiguration());
            return controller;
        }

        //Given:  I have a controller and a basic 
        //When:   I call it
        //Then:   It should return true
        [Test]
        public void ShouldAlwaysPassTest()
        {
            using (var lifetime = _container.BeginLifetimeScope())
            {
                //Given
                var controller = GetAmIHereController(lifetime);

                //When
                var result = controller.GetIAmHere();

                //Then
                Assert.IsTrue(result, "Could not find the controller.");
            }
        }
    }
}