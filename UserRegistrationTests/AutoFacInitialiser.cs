﻿using System.Reflection;
using Autofac;
using Data.Infrastructure;
using Data.Repositories;
using FeelMeApi.Controllers;

namespace FeelMe.Helper
{
    public static class AutoFacInitialiser
    {
        public static IContainer AutoFacSetup()
        {
            var builder = new ContainerBuilder();
            // Register API controllers using assembly scanning.
            builder.RegisterApiControllers(Assembly.GetExecutingAssembly());
            builder.RegisterType<UnitOfWork>().As<IUnitOfWork>().InstancePerLifetimeScope();
            builder.RegisterType<DatabaseFactory>().As<IDatabaseFactory>().InstancePerLifetimeScope();
            builder.RegisterAssemblyTypes(typeof (UserRepository)
                .Assembly).Where(t => t.Name.EndsWith("Repository"))
                .AsImplementedInterfaces().InstancePerLifetimeScope();

            builder.RegisterType<AmIHereController>().As(typeof (AmIHereController));
            return builder.Build();
        }
    }
}