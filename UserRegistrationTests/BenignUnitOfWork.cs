﻿using System.Data.Entity;
using Data.Infrastructure;

namespace UserRegistrationTests
{
    public class BenignUnitOfWork : IUnitOfWork
    {
        private DbContext _dataContext;
        private readonly IDatabaseFactory _databaseFactory;

        public BenignUnitOfWork(IDatabaseFactory databaseFactory)
        {
            _databaseFactory = databaseFactory;
        }

        protected DbContext DataContext
        {
            get { return _dataContext ?? (_dataContext = _databaseFactory.Get()); }
        }

        public void Commit()
        {
            //Do nothing
        }
    }
}